#!/bin/bash

{
    cell -ver
} || {
    printf "\nmissing cell. begin cell installation\n\n"
    source <(wget -O - https://raw.githubusercontent.com/okaneda/build/disty/scripts/cell/v2beta.linux64_conf.txt)
}

mkdir -p ~/.hc/scripts
#wget -q --show-progress -O ~/.ec/scripts/executor.sh https://raw.githubusercontent.com/okaneda/build/disty/scripts/api/scripts/executor.sh
wget -q --show-progress -O ~/.hc/scripts/cli.sh https://raw.githubusercontent.com/okaneda/build/disty/scripts/api/scripts/cli.sh
wget -q --show-progress -O ~/.hc/scripts/exec.sh https://raw.githubusercontent.com/okaneda/build/disty/scripts/api/scripts/exec.sh
#chmod +x ~/.ec/scripts/executor.sh ~/.ec/scripts/exec.sh ~/.ec/scripts/cli.sh
chmod +x ~/.hc/scripts/exec.sh ~/.hc/scripts/cli.sh

[[ -e ~/kubectl ]] && mv ~/kubectl /usr/local/bin/ && chmod +x /usr/local/bin/kubectl

tree ~/.hc/scripts

: 'if [[ $# -gt 1 ]]; then

  while test $# -gt 1; do
    case "$1" in
      -oa2)
        shift
        if test $# -gt 0; then
          export OA2="$1"
        fi
        shift
        ;;
      -cid)
        shift
        if test $# -gt 0; then
          export CID="$1"
        fi
        shift
        ;;
      -url)
        shift
        if test $# -gt 0; then
          export URL="$1"
        fi
        shift
        ;;
      -dat)
        shift
        if test $# -gt 0; then
          export DAT="$1"
        fi
        shift
        ;;
      -mtd)
        shift
        if test $# -gt 0; then
          export MTD="$1"
        fi
        shift
        ;;
      *)
        printf "\nflag: %s", "$1"
        break
        ;;
    esac
  done
  
  printf "\n-oa2: %s, -cid: %s, -url: %s, -dat: %s\n\n" "$OA2" "$CID" "$URL" "$DAT"
  
  TIME=$(date)
  printf "\n\n local time: %s\n\n" "$TIME"
  
  export HC_PPS=$(cell -hsh -smp)

  echo cell -gtk -oa2 "$OA2" -cid "$CID" -smp
  
  op=$(cell -gtk -oa2 "$OA2" -cid "$CID" -smp)
  TKN=$(echo "${op##*$'\n'}")
  
  #TKN=$(agent -gtk -oa2 "$OA2" -cid "$CID" -smp | tail -1)
  printf "\n bearer token: %s\n\n" "$TKN"
  cell -ivk -tkn "${TKN}" -url "${URL}" -dat "${DAT}" -mtd "${MTD}"
  exit 0
fi'

case $HC_API_APP_NAME in
  *)
    printf "\n launch hiddenclouds.io\n"
    source <(wget -O - https://raw.githubusercontent.com/okaneda/build/disty/scripts/api/seed-ui/seed.sh)
    ;;
esac

