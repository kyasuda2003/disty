#!/bin/bash

if [[ ! -z "${HC_PPS}" ]]; then
  export HC_PPS=$(cell -hsh -smp)
fi

cell "$@"
