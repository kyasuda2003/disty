#!/bin/bash
mkdir -p ~/.hc/cell/conf ~/.hc/cell/bin
wget -q --show-progress -O ~/.hc/cell/conf/cell_v2beta.sh https://gitlab.com/kyasuda2003/disty/-/raw/v2beta/scripts/cell/conf/cell_v2beta.sh
chmod 755 ~/.hc/cell/conf/cell_v2beta.sh

# tbc add more conf files

[[ -e "~/.hc/cell" ]] && rm "~/.hc/cell" || { printf "\n no cell binary found!\n\n" }


# default if CELL_REV is present in env
CELL_REV_LOCAL=$CELL_REV
if [[ $CELL_REV_LOCAL != "1.2-b."* ]]; then
  export CELL_REV_LOCAL=cell_v2beta
  if [ ! -f ~/.hc/cell/bin/$ACELL_REV_LOCAL ]; then
    wget -q --show-progress -O ~/.hc/cell/bin/${CELL_REV_LOCAL} https://gitlab.com/kyasuda2003/disty/-/raw/v2beta/${CELL_REV_LOCAL}
    chmod +x ~/.hc/cell/bin/${CELL_REV_LOCAL}
  fi
#else
#  wget -q --show-progress -O ~/.ec/tengu_linux_sys.tar.gz https://raw.githubusercontent.com/EC-Release/tengu/${AGENT_REV_LOCAL}/dist/tengu/tengu_linux_sys.tar.gz
#  tar xvf ~/.ec/tengu_linux_sys.tar.gz -C ~/.ec/agt/bin/ && rm ~/.ec/tengu_linux_sys.tar.gz
#  mv ~/.ec/agt/bin/tengu_linux_sys ~/.ec/agt/bin/${AGENT_REV_LOCAL}
fi

ln -s ~/.hc/cell/bin/${CELL_REV_LOCAL} ~/.hc/cell

tree ~/.hc/cell

export PATH=$PATH:$HOME/.hc
cell -ver
